module.exports = {
    Nothing: 0,
    Tree: 1,
    Player: 2,
    Tavern: 3,
    Spike: 4,
    MineMine: 5,
    MineNotMine: 6,
};
